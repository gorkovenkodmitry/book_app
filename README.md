# Virtualenv #
python2.7
activate virtualenv and run in project dir
```
pip install -r config/requirements.txt
```

# Run app #
```
uwsgi --http :5000 --wsgi-file wsgi.py -p 4 -T --threads 2 -L
```
app runned http://127.0.0.1:5000/

# Testing #
run tests.py in project dir
```
python tests.py
```


# AB testing #

```
ab -c 1000 -k -r -t 10 http://127.0.0.1:5000/

```


## Results ##
Benchmarking 127.0.0.1 (be patient)

Completed 5000 requests

Completed 10000 requests

Finished 13009 requests



Server Software:        

Server Hostname:        127.0.0.1

Server Port:            5000


Document Path:          /

Document Length:        4813 bytes


Concurrency Level:      1000

Time taken for tests:   10.000 seconds

Complete requests:      13009

Failed requests:        0

Keep-Alive requests:    0

Total transferred:      63670940 bytes

HTML transferred:       62617130 bytes

Requests per second:    1300.86 [#/sec] (mean)

Time per request:       768.724 [ms] (mean)

Time per request:       0.769 [ms] (mean, across all concurrent requests)

Transfer rate:          6217.66 [Kbytes/sec] received


Connection Times (ms)

              min  mean[+/-sd] median   max

Connect:        0    0   1.3      0      15

Processing:    26  111 249.1     81    8982

Waiting:       26  111 249.1     80    8982

Total:         37  111 249.5     81    8997


Percentage of the requests served within a certain time (ms)

  50%     81

  66%     84

  75%     87

  80%     88

  90%     94

  95%    277

  98%    292

  99%   1074

 100%   8997 (longest request)
 