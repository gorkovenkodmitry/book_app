# coding: utf-8
import sqlite3
from flask import Blueprint, jsonify, request
from sqlalchemy.orm.exc import NoResultFound
from models import db, Book
from . import forms

mod = Blueprint('api', __name__, url_prefix='/api')


def get_book(title):
    try:
        book = db.session.query(Book).filter(Book.title == title).one()
    except NoResultFound:
        return {}, 404
    return {
        'id': book.id,
        'title': book.title,
        'author': book.author,
        'page_count': book.page_count,
        'pub_date': str(book.pub_date),
    }, 200


def create_book(data):
    form = forms.BookForm(data)
    if form.validate():
        book = Book(
            title=form.title.data,
            author=form.author.data,
            page_count=form.page_count.data,
            pub_date=form.pub_date.data,
        )
        db.session.add(book)
        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            return {
                'title': ['The book already exists'],
            }, 412
        return {
            'id': book.id,
            'title': book.title,
            'author': book.author,
            'page_count': book.page_count,
            'pub_date': str(book.pub_date),
        }, 201
    result = form.errors
    return result, 412


def update_book(data):
    try:
        book = db.session.query(Book).filter(Book.id == data.get('id')).one()
    except NoResultFound:
        return {}, 404
    form = forms.BookForm(data)
    if form.validate():
        book.title = form.title.data
        book.author = form.author.data
        book.page_count = form.page_count.data
        book.pub_date = form.pub_date.data
        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            return {
                'title': ['The book with title already exists'],
            }, 412
        return {}, 204
    result = form.errors
    return result, 412


def delete_book(title):
    try:
        book = db.session.query(Book).filter(Book.title == title).one()
    except NoResultFound:
        return {}, 404
    db.session.delete(book)
    db.session.commit()
    return {}, 204


@mod.route('/book/', methods=['GET', 'POST', 'PUT', 'DELETE'])
def book():
    if request.method == 'GET':
        response = get_book(request.args.get('title'))
        return jsonify(response[0]), response[1]
    if request.method == 'POST':
        response = create_book(request.form.copy())
        return jsonify(response[0]), response[1]
    if request.method == 'PUT':
        response = update_book(request.form.copy())
        return jsonify(response[0]), response[1]
    if request.method == 'DELETE':
        response = delete_book(request.form.get('title'))
        return jsonify(response[0]), response[1]
    return '', 405
