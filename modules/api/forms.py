# coding: utf-8
from wtforms import Form
from wtforms.fields.core import StringField, IntegerField, DateField
from wtforms.validators import InputRequired, Optional


class BookForm(Form):
    title = StringField(validators=[InputRequired()])
    author = StringField(validators=[Optional()])
    page_count = IntegerField(validators=[Optional()])
    pub_date = DateField(validators=[Optional()], format='%d.%m.%Y', render_kw={'placeholder': 'dd.mm.yyyy'})
