# coding: utf-8
from flask import render_template, request
from init import app, db
import models
from modules.api.forms import BookForm


@app.route('/')
def index():
    context = {
        'page_title': u'Main page',
        'form': BookForm()
    }
    try:
        context['limit'] = int(request.args.get('limit', 10))
        if context['limit'] > 10:
            context['limit'] = 50
    except ValueError:
        context['limit'] = 10
    try:
        context['offset'] = int(request.args.get('offset', 0))
        if context['offset'] < 0:
            context['offset'] = 0
    except ValueError:
        context['offset'] = 0
    context['books'] = db.session.query(models.Book).order_by(models.Book.title).offset(context['offset']).limit(context['limit'])
    return render_template('index.html', **context)


from modules.api.views import mod as api_mod
app.register_blueprint(api_mod)


if __name__ == "__main__":
    app.run(debug=True, port=5000)
