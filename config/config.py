#coding: utf-8
import os
ROOT_PATH = os.path.dirname(os.path.dirname(__file__))
STATIC_PATH = os.path.join(ROOT_PATH, 'static')

SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(ROOT_PATH, 'book.db')
#mysql
# SQLALCHEMY_DATABASE_URI = 'mysql://dbuser:dbpass@127.0.0.1:3306/dnname'
SQLALCHEMY_MIGRATE_REPO = os.path.join(ROOT_PATH, 'db_repository')