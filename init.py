from flask import Flask
from config.config import STATIC_PATH, SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, SQLALCHEMY_TRACK_MODIFICATIONS
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__, static_folder=STATIC_PATH)
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_MIGRATE_REPO'] = SQLALCHEMY_MIGRATE_REPO
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = SQLALCHEMY_TRACK_MODIFICATIONS
app.secret_key = '\xa1\xe8q6\xd9\xfb\x95\xb0\xb1a\xe4\xe8\x8c-z\x7f\x87\xfb?\xbb\x9c\x13Y\x10'
db = SQLAlchemy(app)
