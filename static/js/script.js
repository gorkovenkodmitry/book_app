function get_obj(data) {
    var obj;
    if (typeof(data) != 'object') {
        obj = jQuery.parseJSON(data);
    } else {
        obj = data;
    }
    if (obj.debug) {
        console.log(obj.debug);
    }
    return obj;
}

var onReady = {
    init: function() {
        $(document).on('click', '#form-book-add .btn[type="submit"]', onReady.btnBookAddSend);
    },

    btnBookAddSend: function(e) {
        e.preventDefault();
        var btn = $(this);
        var form = $('#form-book-add');
        form.find('.form-group').removeClass('has-error');
        form.find('p.text-danger').hide();
        $.ajax({
            url: form.attr('action'),
            method: 'POST',
            data: form.serialize(),
            success: function(data) {
                var book = get_obj(data);
                var table = $('#books-table');
                table.find('tbody').prepend('<tr><td>'+book.id+'</td><td>'+book.title+'</td><td>'+book.author+'</td><td>'+book.page_count+'</td><td>'+book.pub_date+'</td></tr>');
                form.find('input').val('');
            },
            error: function(data) {
                var obj = data.responseJSON;
                for (var key in obj) {
                    console.log(key);
                    form.find('div[data-field="'+key+'"]').addClass('has-error').find('p.text-danger').show().text(obj[key]);
                }
                console.log(data);
            }
        });
    }
}

$(document).ready(onReady.init);