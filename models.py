# coding: utf-8
from init import db


class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), unique=True, nullable=False)
    author = db.Column(db.String(255), nullable=True)
    page_count = db.Column(db.Integer, nullable=True)
    pub_date = db.Column(db.Date, nullable=True)

    def __unicode__(self):
        return u'%s' % self.title
