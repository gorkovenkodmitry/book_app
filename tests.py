from collections import OrderedDict
import datetime
from flask import json
import main as flaskr
import unittest


class FlaskTestCase(unittest.TestCase):

    def __init__(self, methodName):
        super(FlaskTestCase, self).__init__(methodName)

    def setUp(self):
        flaskr.app.config['TESTING'] = True
        self.app = flaskr.app.test_client()

    def create_book(self, title, author=None, page_count=None, pub_date=None):
        return self.app.post('/api/book/', data={
            'title': title,
            'author': author,
            'page_count': page_count,
            'pub_date': pub_date,
        })

    def create_book_tests(self):
        rv = self.create_book(self.book_title, pub_date='10.12.1967', page_count=2)
        self.assertEqual(rv.status_code, 201)
        rv = self.create_book(self.book_title+'1', author='author')
        self.assertEqual(rv.status_code, 201)

        rv = self.create_book(self.book_title, pub_date='10.12.1967')
        self.assertEqual(rv.status_code, 412)
        rv = self.create_book(self.book_title, page_count='str')
        self.assertEqual(rv.status_code, 412)

    def update_book(self, book_id, title, author=None, page_count=None, pub_date=None):
        return self.app.put('/api/book/', data={
            'id': book_id,
            'title': title,
            'author': author,
            'page_count': page_count,
            'pub_date': pub_date,
        })

    def update_book_tests(self):
        rv = self.update_book(self.book_id, self.book_title)
        self.assertEqual(rv.status_code, 204)
        rv = self.update_book(-1, self.book_title)
        self.assertEqual(rv.status_code, 404)
        rv = self.update_book(self.book_id, self.book_title+'1')
        self.assertEqual(rv.status_code, 412)

    def get_book(self, title):
        return self.app.get('/api/book/?title=%s' % title)

    def get_book_tests(self):
        rv = self.get_book(self.book_title)
        self.assertEqual(rv.status_code, 200)
        self.book_id = json.loads(rv.data).get('id')

        rv = self.get_book(self.book_title+'1')
        self.assertEqual(rv.status_code, 200)
        rv = self.get_book(self.book_title+'2')
        self.assertEqual(rv.status_code, 404)

    def delete_book(self, title):
        return self.app.delete('/api/book/', data={
            'title': title,
        })

    def delete_book_tests(self):
        rv = self.delete_book(self.book_title)
        self.assertEqual(rv.status_code, 204)
        rv = self.delete_book(self.book_title)
        self.assertEqual(rv.status_code, 404)
        rv = self.delete_book(self.book_title+'1')
        self.assertEqual(rv.status_code, 204)
        rv = self.delete_book(self.book_title+'1')
        self.assertEqual(rv.status_code, 404)

    def test_book(self):
        tests = OrderedDict([
            ('create_book_tests', True, ),
            ('get_book_tests', True, ),
            ('update_book_tests', True, ),
            ('delete_book_tests', True, ),
        ])
        self.book_title = 'test title %s' % datetime.datetime.now()
        self.book_id = None
        for k in tests.keys():
            try:
                getattr(self, k)()
            except Exception as e:
                tests[k] = '%s: %s' % (e.__class__.__name__, e.message)
        print 'Test results \n{'
        for k, v in tests.iteritems():
            print '\t%s: %s' % (k, v)
        print '}'

if __name__ == '__main__':
    unittest.main()
